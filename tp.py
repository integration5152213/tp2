import unittest

def calculate_area_rectangle(length: int, long: int):
    if ((type(length) is int or length < 0) and (type(long) is int or long < 0)):
        res = length * long 
    else:
        res = "error"
    return res

def test_area_type(length, long):
    result = calculate_area_rectangle(length, long)
    assert result == "must be an int"
    
def test_area(length, long):
    result = calculate_area_rectangle(length, long)
    assert result > 0

#test_area(-8,1)
#test_area(0,0)
test_area_type(0.85,8.5)